﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ApiUniversal.Models
{
    public partial class universalContext : DbContext
    {
        public universalContext()
        {
        }

        public universalContext(DbContextOptions<universalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Categoria> Categorias { get; set; } = null!;
        public virtual DbSet<Producto> Productos { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Server=CW-DV-JAGG;database=universal;Trusted_Connection=true;");
                optionsBuilder.UseSqlServer("Server=DBuniversal.mssql.somee.com;database=DBuniversal;user id=pruebaUniversal_SQLLogin_1;pwd=3ju463v2t3");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categoria>(entity =>
            {
                entity.ToTable("categorias");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(200)
                    .HasColumnName("descripcion");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.Property(e => e.CategoriaId).HasMaxLength(20);

                entity.Property(e => e.Descripcion).HasMaxLength(100);

                entity.Property(e => e.Imagen).HasMaxLength(250);

                entity.Property(e => e.Nombre).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
