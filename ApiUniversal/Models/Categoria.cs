﻿using System;
using System.Collections.Generic;

namespace ApiUniversal.Models
{
    public partial class Categoria
    {
        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
    }
}
