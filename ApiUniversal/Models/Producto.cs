﻿using System;
using System.Collections.Generic;

namespace ApiUniversal.Models
{
    public partial class Producto
    {
        public int Id { get; set; }
        public string Nombre { get; set; } = null!;
        public string? Descripcion { get; set; }
        public string? CategoriaId { get; set; }
        public string? Imagen { get; set; }
        public int Stock { get; set; }
        public int Precio { get; set; }
    }
}
