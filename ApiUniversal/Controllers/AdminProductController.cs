using ApiUniversal.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;


namespace ApiUniversal.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdminProductController : ControllerBase
    {
        private universalContext _db;

        public AdminProductController(universalContext db, IConfiguration configuration)
        {
            _db = db;
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        [HttpGet]
        [Route("getProducts")]
        public IActionResult GetProduct(string categoriaID, string nombre, string descripcion)
        {
            List<Producto> listaProductos = new List<Producto>();
            {
                using (SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("Conexion")))
                    try
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand("admin_get_products", connection);
                        command.Parameters.AddWithValue("@categoria", categoriaID);
                        command.Parameters.AddWithValue("@nombre", nombre);
                        command.Parameters.AddWithValue("@descripcion", @descripcion);
                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    listaProductos.Add(new Producto()
                                    {
                                        Id = (int)reader["Id"],
                                        Nombre = reader["Nombre"].ToString(),
                                        Descripcion = reader["Descripcion"].ToString(),
                                        CategoriaId = reader["CategoriaId"].ToString(),
                                        Imagen = reader["Imagen"].ToString(),
                                        Stock = (int)reader["Stock"],
                                        Precio = (int)reader["Precio"]

                                    });
                                }
                                return Ok(listaProductos);
                            }
                            else
                            {
                                return Ok(listaProductos);
                            }
                            connection.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(new { error = ex.Message, code = "SERVER_ERROR" });
                    }
            }

        }

        [HttpPut]
        [Route("/editProduct")]
        public async Task<IActionResult> updateProduct(Producto producto)
        {
            try
            {
                Producto _producto = await _db.Productos.FirstOrDefaultAsync(x => x.Id == producto.Id);

                if (_producto != null)
                {
                    _producto.Nombre = producto.Nombre;
                    _producto.Descripcion = producto.Descripcion;
                    _producto.CategoriaId = producto.CategoriaId;
                    _producto.Imagen = producto.Imagen;
                    _producto.Stock = producto.Stock;
                    _producto.Precio = producto.Precio;
                    _db.Productos.Update(_producto);
                    await _db.SaveChangesAsync();
                    return Ok(new { message = "Producto modificado correctamente" });
                }
                else
                {
                    return BadRequest(new { error = "Producto no encontrado" });
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    error = ex.Message
                });
            }

        }

        [HttpDelete("deleteProduct/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Producto _Product = await _db.Productos.FirstOrDefaultAsync(x => x.Id == id);

                if (_Product != null)
                {
                    _db.Productos.Remove(_Product);
                    await _db.SaveChangesAsync();
                    return Ok(new { message = "Producto Eliminado correctamente." });
                }
                else
                {
                    return BadRequest(new { error = "Producto no encontrado" });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { error = ex.Message });
            }
        }

        [HttpPost("CreateNewProduct")]
        public IActionResult PostFrequentQuestions(Producto producto)
        {
            using (SqlConnection connection = new SqlConnection(Configuration.GetConnectionString("Conexion")))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("admin_new_product", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@nombre", producto.Nombre);
                    command.Parameters.AddWithValue("@descripcion", producto.Descripcion);
                    command.Parameters.AddWithValue("@CategoriaId", producto.CategoriaId);
                    command.Parameters.AddWithValue("@imagen", producto.Imagen);
                    command.Parameters.AddWithValue("@Stock", producto.Stock);
                    command.Parameters.AddWithValue("@precio", producto.Precio);

                    if (command.ExecuteNonQuery() > 0)
                    {
                        return Ok(new { message = "Producto Creado correctamente." });
                    }
                    else
                    {
                        return BadRequest(new { error = "Producto ya existente" });
                    }

                }
                catch (Exception e)
                {
                    return BadRequest(new { error = e.Message });
                }
                finally
                {
                    connection.Close();
                }
            }
        }

    }
}